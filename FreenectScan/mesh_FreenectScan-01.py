# -*- coding: utf8 -*-
bl_info = {
    "name": "Freenect Scan (Kinect) capture to Mesh",
    "author": "europrimus@free.fr",
    "version": (0, 1),
    "blender": (2, 7, 7),
    "location": "Property panel : Mesh",
    "description": "Capture 3D data from Kinect sensor to existing Mesh object",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Mesh"}

import bpy, sys, time
from mathutils import Vector
from bpy_extras.object_utils import object_data_add

sys.path.append("/usr/local/lib/python3.5/dist-packages")   #emplacement de freenect (fichier freenect.cpython-35m-x86_64-linux-gnu.so)

bpy.types.Mesh.FreenectScan_libfreenect=True
try:
  import freenect
except:
    print("-"*3,"Erreur","-"*3)
    print("Cant load freenect lib")
    bpy.types.Mesh.FreenectScan_libfreenect=False
    #exit()
print("-"*5,"Blender freencet python","-"*5)

def get_depth(kinect_device):
  CtxPtr=freenect.init()
  print(CtxPtr)
  data = freenect.sync_get_depth()[kinect_device]
  time.sleep(2) #Wait for Kinect full init
  #freenect.set_led(freenect.LED_GREEN)
  #freenect.set_led(freenect.LED_RED)
  data = freenect.sync_get_depth()[kinect_device]
  #freenect.set_led(freenect.LED_GREEN)
  time.sleep(0) #Wait for Kinect full init
  freenect.sync_stop()
  return data


class FreenectScanUI(bpy.types.Panel):
    """Creates a Panel in the mesh context of the properties editor"""
    bl_label = "Freenect scan (Kinect)"
    bl_idname = "MESH_PT_FreenectScan"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "data"
    _context_path = "object.data"
    _property_type = bpy.types.Mesh
    
    #freenect.num_devices() # nombre de kinect
    bpy.types.Mesh.FreenectScan_CreateVertexColor=bpy.props.BoolProperty(name="Vertex color",description="Create vertex color",default=False)
    bpy.types.Mesh.FreenectScan_Device=bpy.props.FloatProperty(name="Kinect device",description="Kinect device number",default=0,min=0,max=10,step=1,precision=0)
    bpy.types.Mesh.FreenectScan_ZDepthMin=bpy.props.FloatProperty(name="Z Depth Min",description="min Depth field of view from kinect",default=0.5,min=0.5,max=3.4,step=0.1,precision=1)
    bpy.types.Mesh.FreenectScan_ZDepthMax=bpy.props.FloatProperty(name="Z Depth Max",description="max Depth field of view from kinect",default=3,min=0.6,max=3.5,step=0.1,precision=1)
    bpy.types.Mesh.FreenectScan_ConDistMax=bpy.props.FloatProperty(name="Max distance",description="max betwen two vertex for face création",default=0.1,min=0.01,max=5.0,step=0.01,precision=2)
    
    #print("Device: ")
    #print(Device)


    def draw(self, context):
        layout = self.layout
        mesh = context.mesh
        if mesh:
           if not mesh.FreenectScan_libfreenect:
              layout.label(text="Lib freenect not found",icon='ERROR')    #erreur lib freenect
           else:
              layout.label(text="Lib freenect OK",icon='INFO')    #erreur lib freenect


              # Chouse Kinect device.
              layout.label(text=" Kinect device (do nothing):")
              row = layout.row()
              row.prop(mesh,"FreenectScan_Device")

              # Z Depth.
              layout.label(text=" Z Depth:")

              row = layout.row(align=True)
              row.prop(mesh,"FreenectScan_ZDepthMin")
              row.prop(mesh,"FreenectScan_ZDepthMax")
              
              # Vertex color
              layout.label(text=" Options:")
              row = layout.row(align=True)
              row.prop(mesh,"FreenectScan_CreateVertexColor") #vertex color
              row = layout.row(align=True)
              row.prop(mesh,"FreenectScan_ConDistMax") #max distance connecting vertex
              
              # Scan
              layout.label(text=" Scan:")
              row = layout.row()
              row.operator("freenectscan.preview")   # live preview
              row = layout.row()
              row.scale_y = 2.0
              row.operator("freenectscan.acquire")   #record
        else:
          layout.label(text="Only for Mesh object",icon='ERROR')

class FreenectScanPreview(bpy.types.Operator):
    bl_idname = "freenectscan.preview"
    bl_label = "Live Preview"
     
    def execute(self, context):
      print("-"*10+" Preview "+"-"*10)
      return{'FINISHED'}            

class FreenectScanAcquire(bpy.types.Operator):
    bl_idname = "freenectscan.acquire"
    bl_label = "Acquire"
     
    def execute(self, context):
      print("-"*10+" Acquire "+"-"*10)
      DepthData=get_depth(0)
      Verts=[]			#Vecteurs des sommets
      Edges=[]			#Aretes
      Faces=[]
      NbPoints=0		#initialisation du nombre de point chargé
      NbEdges=0			#initialisation du d'arrétes créé
      OldPoint=Vector()
      DepthFac=2047/3.5
      for y in range(0,480,1):
        for x in range(0,640,1):
           z = DepthData[y,x]   #a voir, le kinect "ecrase" la profondeur ou pb de perspective
           if context.mesh.FreenectScan_ZDepthMin*DepthFac < z < context.mesh.FreenectScan_ZDepthMax*DepthFac :
               #print("x,y,z:",x,y,z)
               NewPoint=Vector((x-(640/2),y-(480/2),z))
               Verts.append(NewPoint)
               NbPoints+=1
      MeshName="Kinect"
      Mesh = bpy.data.meshes.new(name=MeshName)
      Mesh.from_pydata(Verts, Edges, Faces)
      Mesh.validate(verbose=False)
      bpy.context.scene.cursor_location = (0.0, 0.0, 0.0)
      Object=object_data_add(context, Mesh)
      return{'FINISHED'}
      
      
def register():
    bpy.utils.register_class(FreenectScanUI)
    bpy.utils.register_class(FreenectScanPreview)
    bpy.utils.register_class(FreenectScanAcquire)


def unregister():
    bpy.utils.unregister_class(FreenectScanUI)
    bpy.utils.unregister_class(FreenectScanPreview)
    bpy.utils.unregister_class(FreenectScanAcquire)


if __name__ == "__main__":
    register()
