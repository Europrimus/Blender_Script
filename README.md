# Script Blender (FR)
Mes script python pour blender (https://www.blender.org/)
 -  io_xyz2mesh.py : importe un fichier texte de point sous forme d'un mesh. tien compte du géoréférencement de la scène.
 -  FreenectScan : utilise un Kinect comme Scanner 3D à l'aide de la librairie Freenect (https://github.com/OpenKinect/libfreenect)

# Blender Script (EN)