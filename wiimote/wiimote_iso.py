# -*- coding:iso8859-1 -*-

"""wiimote: librairie pour utiliser la wiimote via wiiosc.
	auteur : europrimus@free.fr
	licence : GPL
Configuration:
host = "127.0.0.1"	adresse du serveur wiiosc
port_out = 57120	port d'�mition de wiiosc
port_in = 57150		port de r�ception de wiiosc
"""

#configuration
host = "127.0.0.1"
port_out = 57120
port_in = 57150
data = {'erreur':0, 0:{'connect':0}, 1:{'connect':0}, 2:{'connect':0}, 3:{'connect':0} }

#liste des erreurs
code_erreur=range(0,101,+1)
code_erreur[0]="pas d'erreur"

#erreur 1 � 9 : probl�me osc
code_erreur[1]="ne re�ois pas de paquet osc"
code_erreur[2]="erreur de socket"
code_erreur[3]="Paquet osc non reconnus"
code_erreur[4]="Wiimote non trouv�"

#erreur 10 � 19 : probl�me de python
code_erreur[11]="Installez la version compl�te de python"
code_erreur[12]="Modules osc non trouv�\nInstallez simpleOSC: http://opensoundcontrol.org/implementation/python-simple-osc"

#erreur 30 � 49 : probl�me de configuation, fonctionement sous linux
code_erreur[31]="Bluetooth non trouv�.\nV�rifiez ou installez Bluez (http://www.bluez.org/)\nou:\nsudo apt-get install bluetooth"
code_erreur[32]="wiiosc non trouv�.\nInstallez wiiosc : http://www.nescivi.nl/wiiosc/\nl'installation de cwiid est ness�ssaire"
code_erreur[33]="wiiosc non lanc�"
code_erreur[34]="Erreur wiiosc"

#erreur 50 � 69 : probl�me configuation, fonctionement sous windows
code_erreur[50]="ne fonctione pas sous Windows (pour l'instant?)"

#erreur 70 � 89 : probl�me configuation, fonctionement sous macOSX
code_erreur[70]="ne fonctione pas sous macosx (pour l'instant?)"

#erreur 90 � 99 : probl�me autre
code_erreur[90]="Syst�me d'exploitation non support�"

def wiiconf(continuer=1,auto=1,bavard=1):
	"""v�rifi la configuration de l'ordinateur et lance wiiosc
	options:
	conti=1		0 pour s'arreter a chaque erreur
	auto=1		tente d'installer automatiquement les programmes nessessaires
	bavard=1	0 pour ne pas renvoyer de messages d'erreurs"""
	global data
	try:
		import os
	except: 
		if bavard==1 : print "Modules socket non trouv�\nInstallez la version compl�te de python"
		data['erreur']=11
		return data
	if bavard==1 : print "\nv�rification du syst�me d'exploitation"
	osname=os.name
	if osname == 'posix':
		data = wiiconf_linux(continuer,auto,bavard)
		return data
	elif osname == 'nt':
		data = wiiconf_windows(continuer,auto,bavard)
		return data
	else:
		if bavard==1 : print "Syst�me d'exploitation non support�"
		data['erreur']=90

def wiiconf_windows(continuer,auto,bavard):
	"""Configuration de glovePIE pour windows"""
	global data
	if bavard==1 : print "sous windows, tout reste � faire, un peut d'aide sur http://www.selectparks.net/modules.php?name=News&file=article&sid=628"
	data['erreur']=50

def wiiconf_linux(continuer,auto,bavard):
	"""Configuration de wiiosc pour linux"""
	global data
	try:
		import os
		from wiimote import host, port_out, port_in
	except: 
		if bavard==1 : print "modules socket non trouv�\ninstaller la version compl�te de python"
		data['erreur']=11
		return data
	if bavard==1 : print "\nle bluetooth est-il install�?"
	#test le bluetooth : renvois 0 si non trouv�
	exit_status=os.system('if (hcitool dev|grep hci >/dev/null); then exit 1; else exit 0; fi;')
	#print "hcitool exit_status:%s"%(exit_status,)
	if exit_status ==0:
		if bavard==1 : print "bluetooth non trouv�\nV�rifiez ou installez Bluez (http://www.bluez.org/)\n\
		ou:\nsudo apt-get install bluetooth"
		data['erreur']=31
		if auto==1 : os.system('sudo apt-get install bluetooth')
		return data
	else:
		if bavard==1 : print "interface bluetooth trouv�"

	if bavard==1 : print "\nwiiosc est-il install�?"
	#v�rifi la pr�sence de l'executable wiiosc : renvois 0 si non trouv�
	exit_status=os.system('if [ -x $(which wiiosc) ]; then exit 1; else exit 0; fi;')
	#print "wiiosc exit_status:%s"%(exit_status,)
	if exit_status ==0:
		if bavard==1 : print "wiiosc non trouv�.\nInstaller wiiosc : http://www.nescivi.nl/wiiosc/\nl'installation de cwiid est ness�ssaire"
		data['erreur']=32
		return data
	else:
		if bavard==1 : print "wiiosc trouv�"

	if bavard==1 : print "\nwiiosc lanc�?"
	#v�rifi si wiiosc est d�j� lanc� : renvois 0 si non trouv�
	wiiosc=os.system('if ( ps ax |grep wiiosc|grep -v grep >/dev/null); then exit 1; else exit 0; fi;')
	#print "wiiosc:%s" % (wiiosc,)
	if wiiosc == 0:
		if bavard==1 : print "non, lancement de wiiosc"
		data['erreur']=33
		#commande pour lancer wiiosc
		wiiosc_cmd='wiiosc "'+str(port_out)+'" "'+str(port_in)+'" "'+host+'" &'
		exit_status=os.system(wiiosc_cmd)
		#print "wiiosc exit_status:%s"%(exit_status,)
		wii_waite(host,port_out,bavard)
		return data
	else:
		if bavard==1 : print "oui\n"
		data['erreur']=0
		return data

def wii_discover(host,port_in,bavard=1):
	"""recherche une nouvelle wiimote"""
	global data
	try:
		import osc
	except: 
		if bavard==1 : print "modules osc non trouv�\ninstaller simpleOSC: http://opensoundcontrol.org/implementation/python-simple-osc"
		data['erreur']=12
		return data
	if bavard==1 : print "discover (%s, %s)" % (host, port_in, )
	osc.init()
	osc.sendMsg("/wii/discover", "", host, port_in)
	wii_waite(host,port_out,bavard)
	return data


def wii_waite(host, port_out,bavard=1):
	"""attend la connection avec wiiosc"""
	import socket	#reseau
	import osc
	erreur=1
	while erreur!=0:
		#print "%s : s.bind((%s,%s))" % (erreur, host, port_out, )
		try:
			s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
			s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
			s.settimeout(10)
			s.bind((host, port_out))
			wiioscdata = s.recvfrom(1024)[0]
		except:
			erreur=1
		else:
			#erreur=0
			dec = osc.decodeOSC(wiioscdata)
			#for key, value in enumerate(dec):
				#print key, value
			#/wii/error
			if dec[0] == '/wii/error':
				return
			#/wii/found
			elif dec[0] == '/wii/found':
				id = dec[2]
				#print "wiioscdata : %s\nid(%s): %s %s" %(wiioscdata,id(id),id, type(id), )
				#wiioscdata = s.recvfrom(1024)[0]
				if type(id) =="int":
					data[id]['connect']=1
					data['erreur']=0
				else :
					if bavard==1 : print "%s\n" % (code_erreur[10],)
					data['erreur']=4
				return
			else:
				return
		print "rien"


#fonction de r�ception des paquet osc
def wii_data(host, port,bavard=1):
	"""r�cup�re les paquets osc et les d�code"""
	#wiioscdata = s.recvfrom(port)	#test pour d�bug
	#print "d�bug:\nwiioscsata: %s" % (wiioscdate,)
	global data
	try:
		import socket	#reseau
		#import string
	except: 
		if bavard==1 : print "modules socket non trouv�\ninstaller la version compl�te de python"
		data['erreur']=11
		return data
	try: import osc
	except: 
		if bavard==1 : print "modules osc non trouv�\ninstaller simpleOSC: http://opensoundcontrol.org/implementation/python-simple-osc"
		data['erreur']=12
		return data
	#print "\n------ ecoute osc (%s:%s)----------" % (host, port, )
	try:
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		s.settimeout(0.5)
		s.bind((host, port))
		wiioscdata = s.recvfrom(1024)[0]
	except socket.timeout:
		if bavard==1 : print "\nne recois pas de paquet osc"
		data['erreur']=1
		return data
		#wiiconf()
		#sys.exit("arret en cour pour d�bogage")
	except socket.error, (errno, strerror):
		if bavard==1 : print "erreur de socket: (%s, %s)" % (errno, strerror,)
		if bavard==1 : print "s.getsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR): %s" % (s.getsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR),)
		#s.close()
		data['erreur']= 2
		return data
	else:
		data['erreur']=0
		dec = osc.decodeOSC(wiioscdata)
		if bavard==1 : print "dec:%s" % (dec,)
		if dec[0] == '/wii/error':
			data['erreur']=34
		elif dec[0] == '/wii/found':
			data[dec[2]]['connect']=1	#dit que la wiimote N� dec[2] est connect�
		else :
			for key, value in enumerate(dec):
				if bavard==1 : print key, value
				if value[0][0:8] == '/wii/ir/':
					#['/wii/ir/valid', ',iii', 0, 1, 1]
					data[value[2]][str(value[0])+"/"+str(value[3])] = value[4]
				else:
				#elif value[0] == '/wii/battery' or value[0] == '/wii/extension':
					# ['/wii/battery', ',if', 0, 0.29326921701431274]
					# ['/wii/extension', ',ii', 0, 0]
					#['/wii/acc/z', ',if', 0, 0.56078433990478516]
					try: data[value[2]][value[0]] = value[3]
					except IndexError: 
						if bavard==1 : print "\npacquet osc non reconnu\ndec:%s\n%s:  '%s' %s\n" % (dec, key, value, type(value), )
						data['erreur']= 3
		return data

# d�but fonction de Julian Olivier
# Copyright Julian Oliver 2007
# There are no restrictions on the use of this file.
def wiidata(host, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    #s.settimeout(0.01)
    s.bind((host, port))
    raw, addr = s.recvfrom(1024)
    dec = OSC.decodeOSC(raw)
    return dec # this is a list containing all our specified wiimote output
# fin fonction de Julian Olivier


#---------------------OPERATION-----------------------------
#
#"/wii/found" informs you of found devices, with an ID, and the address; the ID is later used in the data sent, and needed to send data to the device
#"/wii/connected" informs you of when the device is connected
#"/wii/disconnected" informs you of when the device is connected
#"/wii/error" informs you of an error
#"/wii/quit" informs you when there were no more devices connected and the program has quit
#
#For the data messages, look in the source code... it is quite clear, I think.
#
#Incoming messages:
#"/wii/discover", will look for a new wii device
#"/wii/extension", "i", get the extension type of device "i" (sends a message back "/wii/extension", "ii")
#"/wii/rumble", "ii", the device "i" will turn rumble on or off
#"/wii/leds", "iiiii", the device "i" will turn its leds on or off based on the four states
#"/wii/quit", will quit the application.

def wii_rumble(host,port,wii_id=0):
	import osc
	print "rumble (%s, %s) %s" % (host, port, wii_id,)
	osc.init()
	osc.sendMsg("/wii/rumble", [wii_id], host, port)
	#bundle = osc.createBundle()
	#osc.appendToBundle(bundle, "/wii/rumble", ["ii", 0, 1]) # 1st message appent to bundle
	#osc.sendBundle(bundle, host, port) # send it to a specific ip and port

def wii_led(host,port,wii_id=0):
	import osc
	print "led (%s, %s) %s" % (host, port, wii_id,)
	osc.init()
	#osc.sendMsg("/wii/leds", ["/wii/leds",wii_id], host, port)
	osc.sendMsg("wiiosc", ["/wii/leds","iiiii"], host, port)

def wii_quit(host,port):
	import osc
	osc.init()
	#print "quit (%s %s, %s %s)" % (host,type(host), port,type(port),)
	osc.sendMsg("/wii/quit", "", host, port)

def wii_info(host,port):
	import osc
	osc.init()
	print "info (%s %s, %s %s)" % (host,type(host), port,type(port),)
	osc.sendMsg("/wii/info", "", host, port)


#fin