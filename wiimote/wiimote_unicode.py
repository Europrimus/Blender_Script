# -*- coding:iso 8859-1 -*-

"""wiimote: librairie pour utiliser la wiimote via wiiosc.
Configuration:
host = "127.0.0.1"	adresse du serveur wiiosc
port_out = 57120	port d\'émition de wiiosc
port_in = 57150		port de réception de wiiosc
information envoyé par la wiimote
keys_...	les boutons de la wiimote
acc_.		les inforamtion des accéléromètres"""

#configuration
host = "127.0.0.1"
port_out = 57120
port_in = 57150

def wiiconf():
	"vérifi la configuration de l'ordinateur et lance wiiosc"
	try:
		import os
		import sys
	except: print "modules socket non trouvé\ninstaller la version complète de python"
	print "\nvérification du système d'exploitation"
	osuname=os.uname()
	#print "osuname:%s"%(osuname,)
	if osuname[0] != 'Linux':
		print "programme uniquement compatible linux (pour l'instant?)"
		sys.exit(1)
	else:
		print "linux trouvé"

	print "\nle bluetooth est-il installé?"
	#test le bluetooth : renvois 0 si non trouvé
	exit_status=os.system('if (hcitool dev|grep hci >/dev/null); then exit 1; else exit 0; fi;')
	#print "hcitool exit_status:%s"%(exit_status,)
	if exit_status ==0:
		print "bluetooth non trouvé\nVérifiez ou installez Bluez (http://www.bluez.org/)\n\
		ou:\nsudo apt-get install bluetooth"
		sys.exit(1)
	else:
		print "interface bluetooth trouvé"

	print "\nwiiosc est-il installé?"
	#vérifi la présence de l'executable wiiosc : renvois 0 si non trouvé
	exit_status=os.system('if [ -x $(which wiiosc) ]; then exit 1; else exit 0; fi;')
	#print "wiiosc exit_status:%s"%(exit_status,)
	if exit_status ==0:
		print "wiiosc non trouvé.\nInstaller wiiosc : http://www.nescivi.nl/wiiosc/\nl'installationde wcid est peut être nesséssaire"
		sys.exit(1)
	else:
		print "wiiosc trouvé"

	print "\nwiiosc lancé?"
	#vérifi si wiiosc est déjà lancé : renvois 0 si non trouvé
	wiiosc=os.system('if ( ps ax |grep wiiosc|grep -v grep >/dev/null); then exit 1; else exit 0; fi;')
	#print "wiiosc:%s" % (wiiosc,)
	if wiiosc == 0:
		print "wiiosc non lancé\nlancement de wiiosc"
		#commande pour lancer wiiosc
		wiiosc_cmd='wiiosc "'+str(port_out)+'" "'+str(port_in)+'" "'+host+'" &'
		exit_status=os.system(wiiosc_cmd)
		#print "wiiosc exit_status:%s"%(exit_status,)
	else:
		print "oui\n"

#fonction de réception des paquet osc
def wiidata(host, port):
	"récupère les paquets osc et les décode"
	#wiioscdata = s.recvfrom(port)	#test pour débug
	#print "débug:\nwiioscsata: %s" % (wiioscdate,)
	try:
		import socket	#reseau
		import string
	except: print "modules socket non trouvé\ninstaller la version complète de python"
	try: import osc
	except: print "modules osc non trouvé\ninstaller simpleOSC: http://opensoundcontrol.org/implementation/python-simple-osc"
	#print "\n------ ecoute osc (%s:%s)----------" % (host, port, )
	try:
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		s.settimeout(0.5)
		s.bind((host, port))
		wiioscdata = s.recvfrom(1024)[0]
	except socket.timeout:
		print "\nne recois pas de paquet osc\nvérification de la configuration"
		wiiconf()
		#sys.exit("arret en cour pour débogage")
	except socket.error, (errno, strerror):
		print "erreur de socket: (%s, %s)" % (errno, strerror,)
		print "s.getsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR): %s" % (s.getsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR),)
		#s.close()
		#help(socket.error)
	else:
		#print "-"*10,"wiioscdata (%s):\n%s" % ( type(wiioscdata), wiioscdata, )
		dec = osc.decodeOSC(wiioscdata)
		#print  "-"*10,"dec (%s):\n%s" % ( type(dec), dec, )
		#help(string)
		wiimote_data = {}
		for key, value in enumerate(dec):
			#print key, value
			#nom = value[0]
			#nom = nom[1,len(nom)]
			#print "nom.string.replace( /, _ ) : %s" % ( nom.replace( "/", "_" ), )
			if value[0] == '/wii/ir/valid':
				#['/wii/ir/valid', ',iii', 0, 1, 1]
				wiimote_data[str(value[0])+"/"+str(value[3])] = value[4]
			elif value[0] == '/wii/ir/x':
				#['/wii/ir/x', ',iif', 0, 1, 0.2626953125]
				wiimote_data[str(value[0])+"/"+str(value[3])] = value[4]
			elif value[0] == '/wii/ir/y':
				#['/wii/ir/x', ',iif', 0, 1, 0.2626953125]
				wiimote_data[str(value[0])+"/"+str(value[3])] = value[4]
			else:
				#['/wii/acc/z', ',if', 0, 0.56078433990478516]
				wiimote_data[value[0]] = value[3]
		return wiimote_data

# début fonction de Julian Olivier
# Copyright Julian Oliver 2007
# There are no restrictions on the use of this file.
def wii_data(host, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    #s.settimeout(0.01)
    s.bind((host, port))
    raw, addr = s.recvfrom(1024)
    dec = OSC.decodeOSC(raw)
    return dec # this is a list containing all our specified wiimote output
# fin fonction de Julian Olivier

#fin