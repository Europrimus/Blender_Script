# Copyright Julian Oliver 2007
# There are no restrictions on the use of this file.

import Rasterizer
import GameLogic
import math
import socket
import OSC

cont = GameLogic.getCurrentController() 

actMinus = cont.getActuator('actMinus')
actPlus = cont.getActuator('actPlus')
actQuit = cont.getActuator('act_quit')
actA = cont.getActuator('actA')
actB = cont.getActuator('actB')
actUp = cont.getActuator('actUp')
actDown = cont.getActuator('actDown')
actLeft = cont.getActuator('actLeft')
actRight = cont.getActuator('actRight')
actOne = cont.getActuator('actOne')
actTwo = cont.getActuator('actTwo')

scene = GameLogic.getCurrentScene()

wiimote	 = scene.getObjectList()["OBwiimote"]

Rasterizer.showMouse(1)

def wii_data(host, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    #s.settimeout(0.01)
    s.bind((host, port))
    raw, addr = s.recvfrom(1024)
    dec = OSC.decodeOSC(raw)
    print "------ info wii ----------"
    print  dec
    return dec # this is a list containing all our specified wiimote output

# thanks to the Club Silo guys for the 3x3 orientation breakdown.
def reorient(alpha, beta, gamma):
	a = math.cos(alpha)
	b = math.sin(alpha)
	c = math.cos(beta)
	d = math.sin(beta)
	e = math.cos(gamma)
	f = math.sin(gamma)	
	ad = a*d
	bd = b*d
	matrix = [[c*e, -a*f+b*d*e, b*f+a*d*e], [c*f, a*e+b*d*f, -b*e+a*d*f], [-d, b*c, a*c]]
	return matrix

# lazy loop. input drives the scene. 
# ['/foo/bar', ',iiiiiiiiiiiiiiffffffiiiiiiiiiiii', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 141, 118, 152, 21.738460540771484, -26.387800216674805, 53.871070861816406, 0.37037035822868347, -0.4444444477558136, 0.80769228935241699, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
vide, vide, one, two, a, b, left, right, up, down, home, plus, minus,\
x, y, z, tx, ty, tz, fx, fy, fz, ir1x, ir1y, ir1s, ir2x,\
ir2y, ir2s, ir3x, ir3y, ir3s, ir4x, ir4y, ir4s = wii_data('localhost', 4950)

mult = 180/math.pi

pitch = float(tx)
roll =  float(ty)
yaw = float(tz)

if minus == 1:
	GameLogic.addActiveActuator(actMinus, 1)
if plus == 1:
	GameLogic.addActiveActuator(actPlus, 1)
if home == 1:
	GameLogic.addActiveActuator(actQuit, 1)
if a == 1:
	GameLogic.addActiveActuator(actA, 1)
if b == 1:
    GameLogic.addActiveActuator(actB, 1)
	#orientation is still a bit wonky, but you get the idea.
    wiimote.setOrientation(reorient(pitch/mult, yaw/mult, roll/mult))
if up == 1:
	GameLogic.addActiveActuator(actUp, 1)
if down == 1:
	GameLogic.addActiveActuator(actDown, 1)
if left == 1:
	GameLogic.addActiveActuator(actLeft, 1)
if right == 1:
	GameLogic.addActiveActuator(actRight, 1)
if one == 1:
	GameLogic.addActiveActuator(actOne, 1)
if two == 1:
	GameLogic.addActiveActuator(actTwo, 1)	
